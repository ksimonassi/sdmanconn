package AWS;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;

/**
 * Creates an EC2 instance
 */

// example of region: "us-east-1"

public class CreateInstance
{
    public void createInstance(String name, String ami_id, String instanceType, String accessKey, String secretKey, String region)
    {
        final String USAGE =
                "To run this example, supply an instance name and AMI image id\n" +
                        "Ex: CreateInstance <instance-name> <ami-image-id>\n";

        /*if (args.length != 2) {
            System.out.println(USAGE);
            System.exit(1);
        }*/

        //BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAJJGOT5DZTCQQWLKA", "tuZSiw6af38FL4yUkLsXPweSVC2nSmCoHU7xX5Is");
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);

        //String name = "teste";
        //String ami_id = "0202be73a57c3c205";

        //final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();
        AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();

        RunInstancesRequest run_request = new RunInstancesRequest()
                .withImageId(ami_id)
                .withInstanceType(InstanceType.T2Micro)
                .withMaxCount(1)
                .withMinCount(1);

        RunInstancesResult run_response = ec2.runInstances(run_request);

        String instance_id = run_response.getReservation().getReservationId();

        Tag tag = new Tag()
                .withKey("Name")
                .withValue(name);

        CreateTagsRequest tag_request = new CreateTagsRequest()
                .withTags(tag);

        CreateTagsResult tag_response = ec2.createTags(tag_request);

        System.out.printf(
                "Successfully started EC2 instance %s based on AMI %s",
                instance_id, ami_id);
    }
}



