/*
 * Copyright 2010-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package AWS;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DryRunResult;
import com.amazonaws.services.ec2.model.DryRunSupportedRequest;
import com.amazonaws.services.ec2.model.MonitorInstancesRequest;
import com.amazonaws.services.ec2.model.UnmonitorInstancesRequest;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Toggles detailed monitoring for an EC2 instance
 */
public class MonitorInstance
{
    public void monitorInstance(String instance_id, String accessKey, String secretKey, String region)
    {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);

        //final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();
        AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();

        DryRunSupportedRequest<MonitorInstancesRequest> dry_request =
            () -> {
            MonitorInstancesRequest request = new MonitorInstancesRequest()
                .withInstanceIds(instance_id);

            return request.getDryRunRequest();
        };

        DryRunResult dry_response = ec2.dryRun(dry_request);

        if (!dry_response.isSuccessful()) {
            System.out.printf(
                "Failed dry run to enable monitoring on instance %s",
                instance_id);

            throw dry_response.getDryRunResponse();
        }

        MonitorInstancesRequest request = new MonitorInstancesRequest()
                .withInstanceIds(instance_id);

        ec2.monitorInstances(request);

        System.out.printf(
            "Successfully enabled monitoring for instance %s",
            instance_id);


    }

    public void unmonitorInstance(String instance_id, String accessKey, String secretKey, String region)
    {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);

        //final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();
        AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();

        DryRunSupportedRequest<UnmonitorInstancesRequest> dry_request =
            () -> {
            UnmonitorInstancesRequest request = new UnmonitorInstancesRequest()
                .withInstanceIds(instance_id);

            return request.getDryRunRequest();
        };

        DryRunResult dry_response = ec2.dryRun(dry_request);

        if (!dry_response.isSuccessful()) {
            System.out.printf(
                "Failed dry run to disable monitoring on instance %s",
                instance_id);

            throw dry_response.getDryRunResponse();
        }

        UnmonitorInstancesRequest request = new UnmonitorInstancesRequest()
            .withInstanceIds(instance_id);

        ec2.unmonitorInstances(request);

        System.out.printf(
            "Successfully disabled monitoring for instance %s",
            instance_id);
    }


    /**
     * For metric examples:
     * https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/monitoring_ec2.html
     * CPUUtilization, NetworkIn, NetworkOut, DiskReadOps, DiskWriteOps, DiskReadBytes, DiskWriteBytes
     *
     * For offsetInMilliseconds:
     * 1000 * 60 * 60 * 24 = 24h
     *
     * For period:
     * represents a reading for each period in seconds
     * */
    public void findCloudWatchData(String instance_id, String accessKey, String secretKey, String region, String metricName, long offsetInMilliseconds, int period)  {

        LinkedHashMap<Date,Double> map=new LinkedHashMap<Date,Double>();
        AmazonCloudWatchClient cloudWatch = new AmazonCloudWatchClient(new BasicAWSCredentials(accessKey, secretKey));
        cloudWatch.setEndpoint("monitoring."+ region +".amazonaws.com");
        //long offsetInMilliseconds = 1000 * 60 * 60 * 24; //
        Dimension instanceDimension = new Dimension();
        instanceDimension.setName("InstanceId");
        instanceDimension.setValue(instance_id);

        GetMetricStatisticsRequest request = new GetMetricStatisticsRequest()
                .withStartTime(new Date(new Date().getTime() - offsetInMilliseconds))
                .withNamespace("AWS/EC2")
                .withPeriod(period) //in seconds
                .withMetricName(metricName) //choose a metric
                .withStatistics("Average")
                .withDimensions(Arrays.asList(instanceDimension))
                .withEndTime(new Date());

        GetMetricStatisticsResult getMetricStatisticsResult = cloudWatch.getMetricStatistics(request);
        //System.out.println(getMetricStatisticsResult.toString());

        //To read the Data
        for (Datapoint dp : getMetricStatisticsResult.getDatapoints()) {
            map.put(dp.getTimestamp(), dp.getAverage());   //or getMaximum() or whatever Statistics you are interested in. You can also maintain a list of the statistics you are interested in. Ex: request.setStatistics(list
            System.out.println(dp.getTimestamp().toString() +" / "+ dp.getAverage() +" / "+ dp.getUnit());

        }
    }
}

