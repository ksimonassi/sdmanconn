/*
 * Copyright 2010-2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package AWS;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.ReleaseAddressRequest;
import com.amazonaws.services.ec2.model.ReleaseAddressResult;

/**
 * Releases an elastic IP address
 */
public class ReleaseAddress
{
    public void releaseAddress(String alloc_id, String accessKey, String secretKey, String region)
    {
        final String USAGE =
            "To run this example, supply an allocation ID.\n" +
            "Ex: ReleaseAddress <allocation_id>\n";

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);

        //final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();
        AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();

        ReleaseAddressRequest request = new ReleaseAddressRequest()
            .withAllocationId(alloc_id);

        ReleaseAddressResult response = ec2.releaseAddress(request);

        System.out.printf(
            "Successfully released elastic IP address %s", alloc_id);
    }
}

