package MAPI.Connector;

public class Factory {

    public Connector getConnector (String connectorType) {

        if (connectorType == null) {
            return null;
        }

        if (connectorType.equalsIgnoreCase("AWS")) {
            return new AWSCon();
        } else if (connectorType.equalsIgnoreCase("GoogleCloud")) {
            return new GoogleCloudCon();
        }

        return null;
    }

}
