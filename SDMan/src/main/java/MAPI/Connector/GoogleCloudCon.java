package MAPI.Connector;

import GoogleCloud.GoogleCloud;

public class GoogleCloudCon implements Connector {
    @Override
    public void createInstance() {

    }

    @Override
    public void startInstance() {

    }

    @Override
    public void stopInstance() {

    }

    @Override
    public void monitorInstance() {

    }

    @Override
    public void collectMetric() {
        GoogleCloud gc = new GoogleCloud();
        gc.connect("https://monitoring.googleapis.com/v3/projects/sdman-196518/timeSeries/?filter=metric.type+%3D+%22compute.googleapis.com%2Finstance%2Fcpu%2Futilization%22&interval.endTime=2018-08-09T00%3A00%3A00.000000Z&interval.startTime=2018-08-08T00%3A00%3A00.000000Z");
    }
}
