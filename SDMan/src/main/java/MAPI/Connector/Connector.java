package MAPI.Connector;

public interface Connector {

    public abstract void createInstance();

    public abstract void startInstance();

    public abstract void stopInstance();

    public abstract void monitorInstance();

    public abstract void collectMetric();

}
