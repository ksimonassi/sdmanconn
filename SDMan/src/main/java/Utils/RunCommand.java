package Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RunCommand {
    public String exec(String command) {

        String s = null;
        String result = null;
        //String command = "python /home/ksimonassi/Desenvolvimento/idea/IdeaProjects/SDMan/lib_doc/for_GoogleCloud/googleJWT.py";

        try {

            // run any Unix command, such as "ps -ef"

            Process p = Runtime.getRuntime().exec(command);

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // read the output from the command

            System.out.println("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                result = s;
            }

            // read any errors from the attempted command

            System.out.println("\nHere is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }

            //System.exit(0);
        }
        catch (IOException e) {
            System.out.println("\nexception happened - here's what I know: ");
            e.printStackTrace();
            //System.exit(-1);
        }
        return result;
    }
}
