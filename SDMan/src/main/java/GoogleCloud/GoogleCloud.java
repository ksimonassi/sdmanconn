package GoogleCloud;

import Utils.RunCommand;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class GoogleCloud {
    private static final String PATH = "python /home/ksimonassi/Desenvolvimento/idea/IdeaProjects/SDMan/lib_doc/for_GoogleCloud/googleJWT.py";

    public void connect (String command) {
        RunCommand rc = new RunCommand();
        String token = rc.exec(PATH);

        //String pageUrl = command;

        //String username = "yourUsername";
        //String password = "yourPassword";

        HttpParams httpParams = new BasicHttpParams();
        int timeoutConnection = 10000;
        HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
        int timeoutSocket = 10000;
        HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

        HttpClient httpClient = new DefaultHttpClient(httpParams);

        HttpGet httpGet = new HttpGet(command);
        //String credential = null;

        /*try {
            credential = Base64.getEncoder().encodeToString( ("ya29.c.Elp2BcuVZY_bb1bmIrtkKdZ-e9Xm8tkO4YdAVpoxzZpH5mC9qRx2-AzYR6gZ56eJmgr7YMr3p0e2f86JtneAcU5mWPPFB2KQBpxig7OnHEC6qb19BgOEQcS954Y").getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

        //httpGet.setHeader("Authorization", "Bearer " + credential.substring(0, credential.length()-1));
        httpGet.setHeader("Authorization", "Bearer " + token);
        //httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Connection", "close");

        HttpResponse httpResponse = null;

        try {
            httpResponse = httpClient.execute(httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }

        StatusLine statusLine = httpResponse.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK)  {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            try {
                httpResponse.getEntity().writeTo(outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String responseString = outputStream.toString();
            System.out.println(responseString);
            //processing operations
        }
    }
}
