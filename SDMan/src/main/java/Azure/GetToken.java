package Azure;

import com.google.gson.Gson;
import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.naming.ServiceUnavailableException;
import javax.net.ssl.HttpsURLConnection;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GetToken {
    String redirectUri = "https://login.live.com/oauth20_desktop.srf";

    String authority = "https://login.microsoftonline.com/contoso.onmicrosoft.com/";


    public void getToken () {
        String gateway_url = "https://<GatewayHost>.azurewebsites.net/";
        String app_id_uri = gateway_url + "login/aad";
        String authority = "https://login.microsoftonline.com/ksimonassigmail.onmicrosoft.com";
        String clientId = "2ca52039-1ddc-473f-b1d8-07d3f88effed";
        String clientSecret = "cc7a3c76-c709-46e7-a3ad-66341875bdd4";
        String url = "https://<ApiAppHost>.azurewebsites.net/...";
        /*
         *  Get Access Token from Gateway Login URL with authentication provider name
         *  Note: Please refer to the aad sample in Java for Native Headless at https://github.com/Azure-Samples/active-directory-java-native-headless
         */
        HttpsURLConnection conn = null;
        {
            try {
                conn = (HttpsURLConnection) new URL(app_id_uri).openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        AuthenticationContext context = null;
        AuthenticationResult result = null;
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(1);
            try {
                context = new AuthenticationContext(authority, false, service);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            ClientCredential credential = new ClientCredential(clientId, clientSecret);
            Future<AuthenticationResult> future = context.acquireToken(app_id_uri, credential, null);
            result = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            service.shutdown();
        }

        String accessToken = null;
        if (result == null) {
            try {
                throw new ServiceUnavailableException(
                        "authentication result was null");
            } catch (ServiceUnavailableException e) {
                e.printStackTrace();
            }
        } else {
            accessToken = result.getAccessToken();
            System.out.println("Access Token: " +accessToken);
        }

        /*
         * Using access token to get authentication token
         */
        String data = "{\"access_token\": \""+accessToken+"\"}";
        try {
            conn.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        conn.setDoOutput(true);
        conn.addRequestProperty("Content-Length", data.length()+"");
        try {
            new DataOutputStream(conn.getOutputStream()).writeBytes(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String authTokenResp = null;
        try {
            authTokenResp = IOUtils.toString(conn.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Get Authentication Token Response: " + authTokenResp);
        /*
         * The content of Authentication Token Response is as {"user": {"userId": "sid:xxx...xxx"}, "authenticationToken": "xxxx...xxxxx"}.
         * Need to extract the authenticationToken from Json.
         */
        Gson gson = new Gson();
        Map<String, Object> map = gson.fromJson(authTokenResp, Map.class);
        String authenticationToken = (String) map.get("authenticationToken");
        System.out.println("Authentication Token: "+authenticationToken);
        /*
         * Using authentication token as X-ZUMO-AUTH header to get data from Api App
         * Note: Must using Apache Common HttpClient supported HTTP 30x redirection, Class Http(s)URLConnection not support.
         *          There are three times continuous 302 redirection in accessing Api App with zumo token.
         */
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("x-zumo-auth", authenticationToken);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpResponse resp = null;
        try {
            resp = httpclient.execute(httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String apiAppData = null;
        try {
            apiAppData = IOUtils.toString(resp.getEntity().getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(apiAppData);
    }


}
