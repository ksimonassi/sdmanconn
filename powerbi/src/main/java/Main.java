import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  //private static final Logger LOG = LoggerFactory.getLogger(Main.class);

  public static void main (String[] args) {
    String reportId = "3b08b5f1-b353-4a95-b282-2901bd09b74d";
    String workspaceId = "86d9256e-aff6-4f82-b770-654d5d61d352";
    String dashboardId = "30d934e7-0dac-407e-a64e-561ab93e2d60";

    String accessToken, embedUrl, embedToken;

    GetAccessToken getAccess = new GetAccessToken();
    accessToken = getAccess.token();

    GetEmbedURL getEmbedU = new GetEmbedURL();
    embedUrl = getEmbedU.URL(workspaceId, accessToken, reportId, dashboardId, "report");

    GetEmbedToken getEmbedT = new GetEmbedToken();
    embedToken = getEmbedT.token(workspaceId, accessToken, reportId, dashboardId, "report");

    JSONObject powerbiObj = new JSONObject();
    powerbiObj.put("embedToken", embedToken);
    powerbiObj.put("embedUrl", embedUrl);
    powerbiObj.put("reportId", reportId);
    powerbiObj.put("randomId", String.valueOf(Math.random()).split("\\.")[1]);
    System.out.println(powerbiObj);
  }

}
