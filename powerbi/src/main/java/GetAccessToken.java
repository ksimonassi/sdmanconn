import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;

import javax.naming.ServiceUnavailableException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GetAccessToken {
    private static final String clientId = "13258f5c-403f-4899-975d-3dc3c30d3356";
    private static final String clientSecret = "sO1SxEkyyAQO1mvUBXC+KXVnBXY1evRTrYnDRrySHcM=";
    private static final String tenantId = "6ba1bd5c-750f-4ad6-aba3-0f95585bc21f";

    private String accessToken = "";

    private String authority = String.format("https://login.microsoftonline.com/%s/oauth2/token", tenantId);
    private String resource = "https://analysis.windows.net/powerbi/api";
    private AuthenticationContext context = null;
    private AuthenticationResult result = null;
    private ExecutorService service = null;

    public String token() {
        try {
            service = Executors.newFixedThreadPool(1);
            context = new AuthenticationContext(authority, false, service);
            ClientCredential credential = new ClientCredential(clientId, clientSecret);

            Future<AuthenticationResult> future = context.acquireToken(resource, credential, null);
            try {
                result = future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } finally {
            service.shutdown();
        }
        if (result == null) {
            try {
                throw new ServiceUnavailableException("authentication result was null");
            } catch (ServiceUnavailableException e) {
                e.printStackTrace();
            }
        } else {
            accessToken = result.getAccessToken();
            // System.out.println("Access Token: " + accessToken);
        }
        return accessToken;
    }

}
