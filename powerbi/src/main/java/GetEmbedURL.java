import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.json.*;

public class GetEmbedURL {

    public String URL(String workspaceId, String token, String Id, String dashboardId, String type) {
        String url = "";

        switch (type) {
            case "report":
                url = String.format("https://api.powerbi.com/v1.0/myorg/groups/%s/reports", workspaceId);
                break;
            case "dashboard":
                url = String.format("https://api.powerbi.com/v1.0/myorg/groups/%s/dashboards", workspaceId);
                break;
            case "tiles":
                url = String.format("https://api.powerbi.com/v1.0/myorg/groups/%s/dashboards/%s/tiles", workspaceId,
                        dashboardId);

        }

        String responseString = "";
        String embedUrl = "";

        HttpParams httpParams = new BasicHttpParams();
        int timeoutConnection = 10000;
        HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
        int timeoutSocket = 10000;
        HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

        HttpClient httpClient = new DefaultHttpClient(httpParams);

        HttpGet httpGet = new HttpGet(url);

        httpGet.setHeader("Authorization", "Bearer " + token);
        httpGet.setHeader("Connection", "close");

        HttpResponse httpResponse = null;

        try {
            httpResponse = httpClient.execute(httpGet);
            // System.out.println(httpResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert httpResponse != null;
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            try {
                httpResponse.getEntity().writeTo(outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }

            responseString = outputStream.toString();
            // System.out.println(responseString);
            // processing operations
        }

        JSONObject obj = new JSONObject(responseString);
        JSONArray arr = obj.getJSONArray("value");
        for (int i = 0; i < arr.length(); i++) {
            if (arr.getJSONObject(i).getString("id").equals(Id) && !type.equals("dashboard")) {
                embedUrl = arr.getJSONObject(i).getString("embedUrl");
                break;
            } else if (arr.getJSONObject(i).getString("id").equals(dashboardId)) {
                embedUrl = arr.getJSONObject(i).getString("embedUrl");
            }
        }
        return embedUrl;
    }
}
