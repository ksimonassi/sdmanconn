import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.*;

public class GetEmbedToken {

    public String token(String workspaceId, String token, String Id, String dashboardId, String type) {

        String url = "";

        switch (type) {
            case "report":
                url = String.format("https://api.powerbi.com/v1.0/myorg/groups/%s/reports/%s/GenerateToken", workspaceId,
                        Id);
                break;
            case "dashboard":
                url = String.format("https://api.powerbi.com/v1.0/myorg/groups/%s/dashboards/%s/GenerateToken", workspaceId,
                        dashboardId);
                break;
            case "tiles":
                url = String.format("https://api.powerbi.com/v1.0/myorg/groups/%s/dashboards/%s/tiles/%s/GenerateToken",
                        workspaceId, dashboardId, Id);

        }

        String responseString = "";

        HttpParams httpParams = new BasicHttpParams();
        int timeoutConnection = 10000;
        HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
        int timeoutSocket = 10000;
        HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

        HttpClient httpClient = new DefaultHttpClient(httpParams);

        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Authorization", "Bearer " + token);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("Connection", "close");
        try {
            if (type.equals("report")) {
                httpPost.setEntity(new StringEntity("{\"accessLevel\": \"Edit\",}"));
            } else {
                httpPost.setEntity(new StringEntity("{\"request\": \"\",}"));
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpResponse httpResponse = null;

        try {
            httpResponse = httpClient.execute(httpPost);
            // System.out.println(httpResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert httpResponse != null;
        StatusLine statusLine = httpResponse.getStatusLine();
        if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            try {
                httpResponse.getEntity().writeTo(outputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }

            responseString = outputStream.toString();
            // System.out.println(responseString);
            // processing operations
        }

        JSONObject obj = new JSONObject(responseString);
        String embedToken;
        embedToken = obj.getString("token");
        return embedToken;
    }
}
